# Android Pay

**Official Documentation**: <https://developers.braintreepayments.com/guides/android-pay/overview>

## Installation

In build.gradle:

```
dependencies {
  compile 'com.braintreepayments.api:braintree:2.+'
  compile 'com.google.android.gms:play-services-wallet:8.3.0'
  compile 'com.loopj.android:android-async-http:1.4.9'
}
```

Edit your AndroidManifest.xml to include BraintreeBrowserSwitchActivity and set the android:scheme:

```
<activity android:name="com.braintreepayments.api.BraintreeBrowserSwitchActivity"
    android:launchMode="singleTask"
    android:theme="@android:style/Theme.Translucent.NoTitleBar">
    <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />
        <data android:scheme="${applicationId}.braintree" />
    </intent-filter>
</activity>
```


# 运营 FAQ

### 用户信用卡账单上的扣款公司名称是什么?
扣款公司名称是在申请Braintree账户时提交的商户名称。

### 拒付（chargeback）流程
在用户发起拒付（chargeback）之后，Braintree会给商家发出电子邮件通知。收件邮箱是申请Braintree账户时提交的。商家如果需要申辩，可以根据邮件中的提示提供相应文档给Braintree团队。Braintree团队会帮助商家和银行进行申诉。如果申诉成功，chargeback金额将会返还，而发卡行收取的chargeback费用将不被退还。

### 客户要开charge back的话，有超过某个时间就不能开chargeback的限制？
Visa/MasterCard等卡组织规定用户有权在交易发生后的180天内提出chargeback

### 退款有收费吗？
简单的说，退款没有额外收费，但是整个交易有固定费用。以下是详细说明：

假设Braintree费率为3% ＋ 0.3 USD，原始交易金额为USD 100。交易费用则为：3.3 USD (100 x 3% + 0.3)。当全额退款时，Braintree会发还3%的费用3 USD，而0.3 USD的固定费用不会返还。所以退款本身是免费的，但总体上会有一个固定几毛钱的交易费用。

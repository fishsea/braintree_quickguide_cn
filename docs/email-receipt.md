# 交易邮件通知 (Email Receipt)

Braintree可以对每笔成功交易发送邮件通知。

* 交易需要提供邮箱地址
* 邮件是纯文本形式

如果Braintree的邮件通知不满足要求，商家可以开发自己的邮件通知功能。

**Documentation**

* <https://articles.braintreepayments.com/reference/configuring-spf-records>
* <https://articles.braintreepayments.com/control-panel/transactions/email-receipts>

## 第一步：设置SPF信息

1. 登录DNS修改界面，添加一条TXT／SPF记录
    include:spf.braintreegateway.com
2. 检查SPF记录是否正确
    * <http://www.kitterman.com/spf/validate.html> – 输入domain，不包括“http://”或“www.”。(例如 example.com)
    * 点击“Get SPF Record (if any)”按钮
    * 结果应该类似：
            
            SPF record lookup and validation for: example.com

            SPF records are published in DNS as TXT records.
            The TXT records found for your domain are:
            v=spf1 a mx include:spf.braintreegateway.com -all 

            Checking to see if there is a valid SPF record. 

            Found v=spf1 record for example.com: 
            v=spf1 a mx include:spf.braintreegateway.com -all 

            evaluating...
            SPF record passed validation test with pySPF (Python SPF library)!

## 第二步：通知Braintree开通Email Receipt功能

## 第三步：配置Email Receipt

1. 登录Braintree后台
2. 进入 Settings > Processing 
3. 在 Email Receipts 部分，点击 Edit
4. 选择 Enabled?，并设置发送邮箱地址和邮件模版
5. 如果需要对所有成功交易自动发送通知，开启 Send Receipt by Default? 选项



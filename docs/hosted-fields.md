# Hosted Fields

Hosted Fields提供更有定制灵活性的付款界面的Braintree集成方式，同时兼顾了PCI安全标准条规让您的系统只需达到最基本PCI条规安全使用。本文档介绍网页端Hosted Fields的集成提供参考的代码语言为javascript和ruby。

**文档** 

* <https://developers.braintreepayments.com/guides/hosted-fields/overview>
* <https://www.braintreepayments.com/blog/introducing-hosted-fields-a-pci-solution-that-doesnt-compromise-your-control/>

## 流程

Hosted Fields的对接流程相似与Drop-in UI的流程，除了Hosted Fields付款界面提供单独的输入空间和CSS的定制。更多详情，请参考[Drop-in UI](drop-in.md#flow).


## 用例 - 新用户创建交易

### 生成客户令牌 Client Token
客户令牌(Client token)是Hosted Fields初始化所必需的参数。Hosted Fields每次加载时，一定要使用新生成的客户令牌。

客户令牌需要在服务器端生成，然后在网页端使用。

如果支付用户是一个新用户，生成client token的代码如下：

```ruby
get "/client_token" do
  Braintree::ClientToken.generate
end
```

注意 : 以上的代码是浏览器端通过ajax调用取得客户令牌 。另一种更常见的做法是直接在页面代码中使用，把客户令牌存储在页面变量使用（类似于 <%= client_token %>）。

**文档**

* <https://developers.braintreepayments.com/start/overview#client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#get-a-client-token>
* <https://developers.braintreepayments.com/reference/request/client-token/generate/ruby>

### 客户端

#### 引用 braintree.js

	<script src="https://js.braintreegateway.com/v2/braintree.js"></script>

**文档**

* <https://developers.braintreepayments.com/guides/client-sdk/javascript/v2#use-the-direct-link>

#### 建立 

```html
<form id="checkout" method="post" action="/checkout">
    <div id="paypal-button"></div>
    <div id="card-number" class="bt-input"></div>
    <div id="cvv" class="bt-input"></div>
    <div id="expiration-date" class="bt-input"></div>
	<input type="submit" value="Pay $10">
</form>

<script>
$(function() {
  braintree.setup('<%= your_client_token %>', "custom", {
    id: "new_payment_method",
    hostedFields: {
      onFieldEvent: function (event) {
        if (event.type === "focus") {
          // Handle focus
        } else if (event.type === "blur") {
          // Handle blur
        } else if (event.type === "fieldStateChange") {
          // Handle a change in validation or card type
          console.log(event.isValid); // true|false

          //if (event.isPotentiallyValid)
          console.log("Potentially Valid: " + event.isPotentiallyValid);

          if (!event.isValid) {
            $('.payment-method-icon').removeClass()
                                     .addClass("payment-method-icon");
          }
          if (event.card) {
            console.log(event.card.type);
            // visa|master-card|american-express|diners-club|discover|jcb|unionpay|maestro
            
            $('.payment-method-icon').removeClass()
                                     .addClass("payment-method-icon " + event.card.type);
          }
        }
      },
     styles: {
        // Style all elements
        // these are not all available styles.
        // refer to documentation for more info
        "input": {
          "font-size": "14px",
          "color": "#555555",
          "font-family": "'Helvetica Neue', Helvetica, Arial, sans-serif",
          "font-size": "14px",
          "font-style": "normal",
          "font-variant": "normal",
          "font-weight": "normal"
        },

        // Styling a specific field
        ".number": {
          // "font-family": "monospace"
        },

        // Styling element state
        ":focus": {
          "color": "blue"
        },
        ".valid": {
          "color": "green"
        },
        ".invalid": {
          "color": "red"
        },

        // Media queries
        // Note that these apply to the iframe, not the root window.
        "@media screen and (max-width: 700px)": {
          "input": {
            "font-size": "14px"
          }
        }
    },
      number: {
        selector: "#card-number",
        placeholder: "Credit Card Number"
      },
      cvv: {
        selector: "#cvv",
        placeholder: "CVV"
      },
      expirationDate: {
        selector: "#expiration-date",
        placeholder: "MM/YYYY"
      }
    },
    paypal: {
      container: "paypal-button"
    },
    dataCollector: {
      kount: {environment: 'sandbox'}
    },
    onReady: function (braintreeInstance) {
      var form = document.getElementById('new_payment_method');
      var deviceDataInput = form['device_data'];

      if (deviceDataInput == null) {
        deviceDataInput = document.createElement('input');
        deviceDataInput.name = 'device_data';
        deviceDataInput.hidden = true;
        form.appendChild(deviceDataInput);
      }

      deviceDataInput.value = braintreeInstance.deviceData;
    }
  });
});
</script>
```

```css
#card-number {
  -webkit-transition: border-color 160ms;
  transition: border-color 160ms;
}

.braintree-hosted-fields-focused {
  border-color: blue;
}

.braintree-hosted-fields-invalid {
  border-color: tomato;
}

.braintree-hosted-fields-valid {
  border-color: limegreen;
}

.payment-method-icon {
	background-repeat: no-repeat;
	background-size: 86px auto;
	height: 28px;
	width: 44px;
	display: block;
	margin-top: -31px;
	position: absolute;
	left: auto;
	right: 14px;
	text-indent: -15984px;
}

.visa {
	background-image: url(https://assets.braintreegateway.com/dropin/1.4.0/images/2x-sf9a66b4f5a.png);
	background-position: 0px -184px;
}

.master-card {
	background-image: url(https://assets.braintreegateway.com/dropin/1.4.0/images/2x-sf9a66b4f5a.png);
	background-position: 0px -128px;
}

.american-express {
	// set amex image background
}
```

**文档**

* 建立 Setup - <https://developers.braintreepayments.com/guides/hosted-fields/setup-and-integration/javascript/v2>
* 设置选项 Configuration Options - <https://developers.braintreepayments.com/guides/hosted-fields/configuration/javascript/v2>
* 设计 Styling - <https://developers.braintreepayments.com/guides/hosted-fields/styling/javascript/v2>
* 项目 Events - <https://developers.braintreepayments.com/guides/hosted-fields/events/javascript/v2>


### 服务器端

#### 创建新交易

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_nonce => <nonce>,
	:options => {
	  :submit_for_settlement => true,
	  :store_in_vault_on_success => true
	},
	:device_data => params[:device_data]
)
```

* **[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success)** - when this option is enabled, a new customer record and the payment method used for this transaction will be saved into Braintree vault. Braintree will generate a customer id and a payment method token and return them in the response.
* **[submit_for_settlement](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.submit_for_settlement)** - enable this option to deduct fund from customer's credit card. Refer to <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement> for more information.

**文档**

* <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement>




#### 处理交易结果

交易成功时交易资料可以在返回的交易结果中提取

```rails
if result.success?
	transaction = result.transaction
	id = transaction.id
	braintree_customer_id = transaction.customer.id
	payment_method_token = transaction.payment_method_token
	
	#
	# save transaction id, customer id, payment method token
	#
end
```

* **[transaction id](https://developers.braintreepayments.com/reference/response/transaction/ruby#id)** - 交易ID
* **[customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id)** - 在选择存入客户资料在Braintree的情况，Braintree会自动生成客户ID
* **[payment method token](https://developers.braintreepayments.com/reference/response/transaction/ruby#credit_card_details.token)** - 存入的付款方式会Braintree会给予付款方式令牌

如果交易失败，您可以在返回的交易结果中提取错误信息。我们建议把错误信息显示给客户，然他们可以再重试或使用另一个付款方式。

```ruby
if !result.success?
	response_code = result.error
	...
end
```

## 用例 - 已在用户创建交易

如果已经把用户资料和付款方式存入Braintree [customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id), .

### 生成客户令牌 Client Token

生成客户令牌的同时如果有把客户id加入，可以从中提取已存的付款方式。 

```rails
get "/client_token" do
  @client_token = Braintree::ClientToken.generate(
	:customer_id => <a_customer_id>
  )
end
```

### 显示已存付款方式 （Display the Payment Methods）

在付款时，显示默认付款方式同时提供选择让顾客可以转换付款方式。

在服务器端提取顾客已存的付款方式令牌，
To obtain all available payment methods, on the server side:

```ruby
# 提取客户信息
customer = Braintree::Customer.find("a_customer_id")

# 提取有效的以存付款方式令牌
@payment_methods = customer.payment_methods
```

在客户端显示已存付款方式令牌:

```erb
<ul>
	<% @payment_methods.each do |payment_method| %>
		<li>
			<%= radio_button_tag(:payment_method, payment_method.token) %>
			<%= image_tag payment_method.image_url %>
			<% case payment_method 
				when Braintree::CreditCard %>
				<%= "#{payment_method.last_4} #{payment_method.expiration_month}/#{payment_method.expiration_year}" %>
			<% 	when Braintree::PayPalAccount %>
				<%= "#{payment_method.email}" %>
			<% end %>
		</li>
	<% end %>
</ul>
```

**文档 Documentation**

* **Customer.find()** - <https://developers.braintreepayments.com/reference/request/customer/find/ruby>
* **Customer** - <https://developers.braintreepayments.com/reference/response/customer/ruby>
* **PaymentMethod** - <https://developers.braintreepayments.com/reference/response/payment-method/ruby>

### 服务器端

#### 创建新交易

可以使用顾客选择的已存付款方式令牌（payment method token）创建新的交易。由于已经有了相同的付款方式令牌，创建交易时无需再加入[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success) 。

```ruby
result = Braintree::Transaction.sale(
  :amount => <price>,
  :payment_method_token => <payment method token selected>,
  :options => {
    :submit_for_settlement => true
  },
  :device_data => params[:device_data]
)
```

#### 处理交易结果

无需提取 customer id 和 payment method token.

```ruby
if result.success?
  transaction = result.transaction
  id = transaction.id
  
  #
  # save transaction id
  # process transaction data
  #
end
```
# Vault实现

通过Braintree vault，用户信息和信用卡信息可以安全存储在Braintree。Vault也支持一键支付的实现。

## 数据库设置

网站用户表里加一个字段 _braintree_customer_id_

## 用户第一次支付

1. 客户端请求clientToken
2. 服务器调用$clientToken = Braintree_ClientToken::generate()
3. 客户端使用clientToken创建客户端表单
4. 用户填写信息，提交
5. 服务器调用Transaction.sale()，并设置submitForSettlement, storeInVaultOnSuccess等参数

		$result = Braintree_Transaction::sale([
		  'amount' => '100.00',
		  'paymentMethodNonce' => nonceFromTheClient,
		  'options' => [
		    'submitForSettlement' => True，
		    'storeInVaultOnSuccess' => True
		  ],
		  'deviceData' => ...
		]);

6. 服务器会返回交易结果，其中包括Braintree生成的customer id (<https://developers.braintreepayments.com/reference/response/transaction/php#customer_details.id>)
7. 将customer id存入数据库中的字段 _braintree_customer_id_
8. 返回数据中还包括Payment Method Token等数据，有需要可以存储

## 用户后续支付

1. 服务器端调用$customer = Braintree_Customer::find(braintree_customer_id)
2. 从$customer中得到所有的支付方式：$customer->paymentMethods (<https://developers.braintreepayments.com/reference/response/customer/php#payment_methods>)。 注意有可能信用卡和Paypal支付方式都有，需要区分。
3. 在客户端显示支付方式，供用户选择
4. 用户填写信息，提交。服务器端得到用户选择的支付方式（主要需要payment method token)
5. 服务器调用Transaction.sale()，并设置submitForSettlement, customerId (<https://developers.braintreepayments.com/reference/request/transaction/sale/php#customer_id>) 等参数

		$result = Braintree_Transaction::sale([
		  'amount' => '100.00',
		  'paymentMethodToken' => tokenSelectedByCustomer,
		  'customerId' => braintree_customer_id,
		  'options' => [
		    'submitForSettlement' => True
		  ],
		  'deviceData' => ...
		]);

6. 服务器会返回交易结果

## 读取所有支付方式

1. 读取用户记录

		$customer = Braintree_Customer::find('a_customer_id');

2. 从$customer中得到所有的支付方式

		$customer->paymentMethods 
	
	(<https://developers.braintreepayments.com/reference/response/customer/php#payment_methods>)。 注意有可能信用卡和Paypal支付方式都有，需要区分。

## 删除支付方式

```php
$result = Braintree_PaymentMethod::delete('the_token');

$result->success
#=> true
```

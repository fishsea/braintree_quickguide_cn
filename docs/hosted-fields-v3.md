# Hosted Fields

Hosted Fields提供更有定制灵活性的付款界面的Braintree集成方式，同时兼顾了PCI安全标准条规让您的系统只需达到最基本PCI条规安全使用。本文档介绍网页端Hosted Fields的集成，提供参考的代码语言为javascript和ruby。

**文档** 

* <https://developers.braintreepayments.com/guides/hosted-fields/overview>
* <https://www.braintreepayments.com/blog/introducing-hosted-fields-a-pci-solution-that-doesnt-compromise-your-control/>

## 流程

Hosted Fields的对接流程相似与Drop-in UI的流程，但是Hosted Fields可以定制付款界面和用户体验。更多详情，请参考[Drop-in UI](drop-in.md#flow).


## 用例 - 新用户创建交易

### 生成 Client Token
 Client token是Hosted Fields初始化所必需的参数。Hosted Fields每次加载时，一定要使用新生成的client token。

Client token需要在服务器端生成，然后在网页端使用。

如果支付用户是一个新用户，生成client token的代码如下：

```ruby
get "/client_token" do
  Braintree::ClientToken.generate
end
```

注意 : 以上的代码是浏览器端通过ajax调用取得client token。另一种更常见的做法是直接在页面代码中使用，把client token存储在页面变量使用（类似于 <%= client_token %>）。

**文档**

* <https://developers.braintreepayments.com/start/overview#client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v3#get-a-client-token>
* <https://developers.braintreepayments.com/reference/request/client-token/generate/ruby>

### 客户端

#### 引用 braintree.js

    <!-- Load the Client component. -->
    <script src="https://js.braintreegateway.com/web/3.6.2/js/client.min.js"></script>

    <!-- Load the Hosted Fields component. -->
    <script src="https://js.braintreegateway.com/web/3.6.2/js/hosted-fields.min.js"></script>

**文档**

* <https://developers.braintreepayments.com/start/hello-client/javascript/v3>

#### 代码

```html
<form id="checkout-form" action="/transaction-endpoint" method="post">
  <div id="error-message"></div>

    <label for="card-number">Card Number</label>
    <div class="hosted-field" id="card-number"></div>

    <label for="cvv">CVV</label>
    <div class="hosted-field" id="cvv"></div>

    <label for="expiration-date">Expiration Date</label>
    <div class="hosted-field" id="expiration-date"></div>

    <input type="hidden" name="payment-method-nonce">
    <input type="submit" value="Pay $10" disabled>
</form>

<script>
// We generated a client token for you so you can test out this code
// immediately. In a production-ready integration, you will need to
// generate a client token on your server (see section below).
var authorization = 'eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJjZmVkMmY0NDY1MzMzNDU5NTM0NDY1OTg4OTUyNDAwODM1ZDBkZmJiNjdiYTIwMDMxY2ZkNGMxZDQ3NDU5MWE0fGNyZWF0ZWRfYXQ9MjAxNi0xMS0wOVQwODozOToyNC4wNDQzNjQ2MjArMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJjb2luYmFzZUVuYWJsZWQiOmZhbHNlLCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=';
var submit = document.querySelector('input[type="submit"]');

braintree.client.create({
  authorization: authorization
}, function (clientErr, clientInstance) {
  if (clientErr) {
    // Handle error in client creation
    return;
  }

  braintree.hostedFields.create({
    client: clientInstance,
    styles: {
      'input': {
        'font-size': '14pt'
      },
      'input.invalid': {
        'color': 'red'
      },
      'input.valid': {
        'color': 'green'
      }
    },
    fields: {
      number: {
        selector: '#card-number',
        placeholder: '4111 1111 1111 1111'
      },
      cvv: {
        selector: '#cvv',
        placeholder: '123'
      },
      expirationDate: {
        selector: '#expiration-date',
        placeholder: '10/2019'
      }
    }
  }, function (hostedFieldsErr, hostedFieldsInstance) {
    if (hostedFieldsErr) {
      // Handle error in Hosted Fields creation
      return;
    }

    submit.removeAttribute('disabled');

    form.addEventListener('submit', function (event) {
      event.preventDefault();

      hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
        if (tokenizeErr) {
          // Handle error in Hosted Fields tokenization
          return;
        }

        // Put `payload.nonce` into the `payment-method-nonce` input, and then
        // submit the form. Alternatively, you could send the nonce to your server
        // with AJAX.
        document.querySelector('input[name="payment-method-nonce"]').value = payload.nonce;
        form.submit();
      });
    }, false);

  });
});
</script>
```

```css
#card-number {
  border: 1px solid #333;
  -webkit-transition: border-color 160ms;
  transition: border-color 160ms;
}

#card-number.braintree-hosted-fields-focused {
  border-color: #777;
}

#card-number.braintree-hosted-fields-invalid {
  border-color: tomato;
}

#card-number.braintree-hosted-fields-valid {
  border-color: limegreen;
}

```

**文档**

* Setup - <https://developers.braintreepayments.com/guides/hosted-fields/setup-and-integration/javascript/v3>
* 设置选项 Configuration Options - <https://braintree.github.io/braintree-web/3.6.2/index.html>
* 样式 Styling - <https://developers.braintreepayments.com/guides/hosted-fields/styling/javascript/v3>
* 回调事件 Events - <https://developers.braintreepayments.com/guides/hosted-fields/events/javascript/v3>


### 服务器端

#### 创建新交易

```ruby
result = Braintree::Transaction.sale(
  :amount => <price>,
  :payment_method_nonce => <nonce>,
  :options => {
    :submit_for_settlement => true,
    :store_in_vault_on_success => true
  },
  :device_data => params[:device_data]
)
```

* **[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success)** - when this option is enabled, a new customer record and the payment method used for this transaction will be saved into Braintree vault. Braintree will generate a customer id and a payment method token and return them in the response.
* **[submit_for_settlement](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.submit_for_settlement)** - enable this option to deduct fund from customer's credit card. Refer to <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement> for more information.

**文档**

* <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement>



#### 处理交易结果

交易成功时交易资料可以在返回的交易结果中提取

```rails
if result.success?
  transaction = result.transaction
  id = transaction.id
  braintree_customer_id = transaction.customer.id
  payment_method_token = transaction.payment_method_token
  
  #
  # save transaction id, customer id, payment method token
  #
end
```

* **[transaction id](https://developers.braintreepayments.com/reference/response/transaction/ruby#id)** - 交易ID
* **[customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id)** - 在选择存入客户资料在Braintree的情况，Braintree会自动生成客户ID
* **[payment method token](https://developers.braintreepayments.com/reference/response/transaction/ruby#credit_card_details.token)** - 存入的付款方式会Braintree会给予付款方式令牌

如果交易失败，您可以在返回的交易结果中提取错误信息。我们建议把错误信息显示给客户，然他们可以再重试或使用另一个付款方式。

```ruby
if !result.success?
  response_code = result.error
  ...
end
```

## 用例 - 已在用户创建交易

如果已经把用户资料和付款方式存入Braintree [customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id), .

### 生成Client Token

生成Client Token的同时如果有把csutomer_id加入，可以从中提取已存的付款方式。 

```rails
get "/client_token" do
  @client_token = Braintree::ClientToken.generate(
  :customer_id => <a_customer_id>
  )
end
```

### 显示已存付款方式 （Display the Payment Methods）

在付款时，显示默认付款方式同时提供选择让顾客可以转换付款方式。

在服务器端提取顾客已存的payment method token，
To obtain all available payment methods, on the server side:

```ruby
# 获取客户信息
customer = Braintree::Customer.find("a_customer_id")

# 提取已经保存的支付方式信息
@payment_methods = customer.payment_methods
```

在客户端显示已经保存的支付方式信息:

```erb
<ul>
  <% @payment_methods.each do |payment_method| %>
    <li>
      <%= radio_button_tag(:payment_method, payment_method.token) %>
      <%= image_tag payment_method.image_url %>
      <% case payment_method 
        when Braintree::CreditCard %>
        <%= "#{payment_method.last_4} #{payment_method.expiration_month}/#{payment_method.expiration_year}" %>
      <%  when Braintree::PayPalAccount %>
        <%= "#{payment_method.email}" %>
      <% end %>
    </li>
  <% end %>
</ul>
```

**文档 Documentation**

* **Customer.find()** - <https://developers.braintreepayments.com/reference/request/customer/find/ruby>
* **Customer** - <https://developers.braintreepayments.com/reference/response/customer/ruby>
* **PaymentMethod** - <https://developers.braintreepayments.com/reference/response/payment-method/ruby>

### 服务器端

#### 创建新交易

可以使用顾客选择的信用卡来创建新的交易。由于已经有了payment method token，创建交易时无需再加入[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success) 。

```ruby
result = Braintree::Transaction.sale(
  :amount => <price>,
  :payment_method_token => <payment method token selected>,
  :options => {
    :submit_for_settlement => true
  },
  :device_data => params[:device_data]
)
```

#### 处理交易结果

```ruby
if result.success?
  transaction = result.transaction
  id = transaction.id
  
  #
  # save transaction id
  # process transaction data
  #
end
```


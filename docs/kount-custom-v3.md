# Kount Custom集成

## Kount Merchant ID

Braintree会提供一个**Kount Merchant ID**。这个**Kount Merchant ID**在sandbox和production是相同的。

## 客户端 － 收集设备指纹

```html
<script src="https://js.braintreegateway.com/web/3.6.2/js/client.min.js"></script>
<script src="https://js.braintreegateway.com/web/3.6.2/js/data-collector.min.js"></script>

<script>
//**** NOTED Your Direct Kount MID should be configured in your Braintree Control Panel, Please reach out to support@braintreepayments.com **** 
braintree.client.create({
  authorization: 'CLIENT_AUTHORIZATION'
}, function (err, clientInstance) {
  // Creation of any other components...

  braintree.dataCollector.create({
    client: clientInstance,
    kount: true
  }, function (err, dataCollectorInstance) {
    if (err) {
      // Handle error in creation of data collector
      return;
    }
    // At this point, you should access the dataCollectorInstance.deviceData value and provide it
    // to your server, e.g. by injecting it into your form as a hidden input.
    var form = document.getElementById('my-form-id');
    var deviceDataInput = form['device_data'];

    if (deviceDataInput == null) {
      deviceDataInput = document.createElement('input');
      deviceDataInput.name = 'device_data';
      deviceDataInput.type = 'hidden';
      form.appendChild(deviceDataInput);
    }

    deviceDataInput.value = dataCollectorInstance.deviceData;
  });
});
</script>
```

**注**：移动端有相应的集成。请参考Braintree官方文档。

**Documentation**

* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/client-side/javascript/v3#kount-custom-integration>

## 服务器端－device_data

1. 使用**payment_method_nonce**创建交易

		result = Braintree::Transaction.sale(
		  :amount => "1000.00",
		  :payment_method_nonce => nonce_from_the_client,
		  :options => {
		    :submit_for_settlement => false
		  },
		  :device_data => params[:device_data],
	      :customer => {
	        :firstName => "Drew",
	        :lastName => "Smith",
	        :company => "Braintree",
	        :phone => "312-555-1234",
	        :website => "http://www.example.com",
	        :email => "drew@example.com"
	      },
	      billing: {
	        :firstName => "Paul",
	        :lastName => "Smith",
	        :company => "Braintree",
	        :streetAddress => "1 E Main St",
	        :extendedAddress => "Suite 403",
	        :locality => "Chicago",
	        :region => "IL",
	        :postalCode => "60622",
	        :countryCodeAlpha2 => "US"
	      },
	      shipping: {
	        :firstName => "Paul",
	        :lastName => "Smith",
	        :company => "Braintree",
	        :streetAddress => "1 E Main St",
	        :extendedAddress => "Suite 403",
	        :locality => "Chicago",
	        :region => "IL",
	        :postalCode => "60622",
	        :countryCodeAlpha2 => "US"
	      }
		)

2. 使用**payment_method_token**创建交易

		result = Braintree::Transaction.sale(
		  :amount => "1000.00",
		  :payment_method_token => paypal_method_token,
		  :options => {
		    :submit_for_settlement => false
		  },
		  :device_data => params[:device_data],
	      :billing_address_id => billing_address_id,
	      :shipping_address_id => shipping_address_id
		)

3. 绑卡

		result = Braintree::PaymentMethod.create(
          :customer_id => "131866",
          :payment_method_nonce => nonce_from_the_client,
          :options => {
            :verify_card => true
          },
          :device_data => params[:device_data],
          :billing_address => {
            :firstName => "Paul",
            :lastName => "Smith",
            :company => "Braintree",
            :streetAddress => "1 E Main St",
            :extendedAddress => "Suite 403",
            :locality => "Chicago",
            :region => "IL",
            :postalCode => "60622",
            :countryCodeAlpha2 => "US"
          }
        )

**Documentation**

* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/server-side/ruby#using-device-data>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#billing>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#billing_address_id>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#customer>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#customer_id>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#shipping>
* <https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#shipping_address_id>

## 开启Advanced Fraud Tools

在Braintree后台开启Advanced Fraud Tools：Settings -> Processing -> Advanced Fraud Tools

![Advanced Fraud Tools](img/config-advanced-fraud-tools.png)

## 自定义字段（UDF）

Kount支持自定义字段（User Defined Fields)用于定义风控规则。自定义字段数值可以在Braintree的API调用中传入，但是首先要先在Braintree和Kount中都创建相应的自定义字段。

### Braintree中自定义字段的命名规则

字段类型      | 前缀           | UDF名称      | 结果
------------ | ------------- | ------------ | ------------ 
Text | fraud_  | name | fraud_name
Numeric | fraud_numeric_  | customer_id | fraud_numeric_customer_id
Date | fraud_date_  | order_date | fraud_date_order_date

* Date样式： MM/DD/YYYY

### Braintree中自定义字段的创建

1. Settings -> Processing

	![Processing Option](img/config-settings-processing.png)

2. 在"Custom Fields"部分，点击"New"

	![Custom Field Configuration](img/config-custom-fields.png)

3. 填写字段信息

	* **Api name**: 字段名
	* **Display name**: 字段标签
	* **Store and Pass Back**: 开启

	![New Custom Field](img/config-custom-fields-new.png)

### Kount中自定义字段的创建

在Kount中创建相同的字段，但是去掉**fraud_**前缀。

1. Login -> FRAUD CONTROL -> User Defined Fields
	![User Defined Fields Menu](img/kount-config-udf-menu.png)
2. Go to the bottom of the list of User Defined Fields
	![List of User Defined Fields](img/kount-config-udf-list.png)
	![Button to Add User Defined Fields](img/kount-config-udf-add.png)
3. Enter UDF information
	* **Label**: enter the API name of the corresponding Braintree custom field, but omit the prefix **fraud_**
	* **Discription**: description of the field
	* **Type**: field data type

	![Enter UDF information](img/kount-config-udf-new.png)

### 自定义字段传递

```
result = Braintree::Transaction.sale(
  :amount => "1000.00",
  :payment_method_nonce => nonce_from_the_client,
  :options => {
    :submit_for_settlement => false
  },
  :custom_fields => {
    :fraud_name => customer_name,
    :fraud_numeric_customer_id => customer_id,
    :fraud_date_order_date => order_date
  }
  :device_data => params[:device_data]
)
```

## 风控流程

Kount会实时返回4种结果

* **Not Evaluated** - Kount没有进行风控（比如因为超时），Braintree会批准交易
* **Approve** - Kount批准交易，Braintree也会批准交易
* **Review/Escalate** - Kount建议审查交易，Braintree会批准交易。此时交易应该被列入人工审查
* **Decline** - Kount建议拒绝交易，Braintree会拒绝交易。

### 流程图
[![Kount Direct 流程图](img/kount-flow.png)](img/kount-flow.png)

代码大致流程如下：

```ruby
# 创建授权交易
result = Braintree::Transaction.sale(
  :amount => "1000.00",
  :payment_method_nonce => nonce_from_the_client,
  :options => {
    :submit_for_settlement => false
  },
  :custom_fields => {
    :fraud_name => customer_name,
    :fraud_numeric_customer_id => customer_id,
    :fraud_date_order_date => order_date
  }
  :device_data => params[:device_data]
)

if result.success?
	transaction_id = result.transaction.id
	
	case result.transaction.status
	when 'authorized':	# 授权成功，检查Kount结果
		decision = result。transaction.risk_data.decision

		# 根据Kount结果处理
		case decision
		when 'Approve', 'Not Evaluated' # 批准交易，进行扣款
			settlement_result = Braintree::Transaction.submit_for_settlement(transaction_id)
			...
		when 'Review', 'Escalate'       # 人工审查交易
			# 将订单状态设成"ON HOLD"
			# 发送通知给风控人员进行人工审查
			...
		else
			# 处理无效状态
			# Kount Decision = 'Declined'的情况不应该出现，因为交易结果会失败
			# Kount Decision 为空可能出现，因为某些支付方式，比如Apple Pay不支持Kount

		end
	when 'failed', 'gateway_rejected', 'processor_declined'
		# 交易失败
		# 处理订单
		...
	end
end
```

```ruby
# 人工审查处理后
review_result = order.review_result # 获取人工审查结果
transaction = order.transaction     # 获取支付信息
transaction_id = transaction.id

case review_result
when 'APPROVE'    # 批准交易，进行扣款
	settlement_result = Braintree::Transaction.submit_for_settlement(transaction_id)
	...
when 'DECLINE'    # 拒绝交易，取消授权
	void_result = Braintree::Transaction.void(transaction_id)
	...
else
	# 处理其他状态
end
```

## 其他注意事项

### 用户邮箱地址

如果用户邮箱地址为空，则不要传入[customer_email](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#customer.email)这个参数

**Documentation**

* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/server-side/ruby#customer-email-address>

# Drop-in UI

Drop-in UI是最简单的Braintree集成方式。本文档介绍网页端Drop-in UI的集成，代码语言为javascript和ruby。

**演示**

* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#demo>

**文档** 

* <https://developers.braintreepayments.com/start/hello-client/javascript/v2>
* <https://developers.braintreepayments.com/guides/drop-in/javascript/v2>

## 流程简述

1. 浏览器加载支付页面
2. 服务器生成 _client_token_（一次性的客户端令牌），和支付页面一起返回
3. 浏览器初始化Braintree client SDK，并加载Drop-in UI表单
4. 用户填写信用卡信息并提交表单
5. Braintree client SDK拦截表单提交事件，通过ajax将信用卡信息传送到Braintree服务器，然后得到 _payment_method_nonce_（信用卡信息加密结果）
6. Braintree client SDK在表单中自动添加隐藏字段 _payment_method_nonce_，然后继续表单提交
7. 商户服务器收到表单数据，其中包含 _payment_method_nonce_，但不包括信用卡信息
8. 商户服务器调用Braintree API  _Transaction.sale_ 创建新交易（其中使用 _payment_method_nonce_ 参数）
9. Braintree处理交易，并返回结果

**文档**

* <https://developers.braintreepayments.com/start/overview#how-it-works>
* Client Token - <https://developers.braintreepayments.com/reference/request/client-token/generate/php>
* Payment Method Nonce - <https://developers.braintreepayments.com/start/hello-client/javascript/v2#send-payment-method-nonce-to-server>


## 用例 - 新用户创建交易

### 生成 Client Token
Client token 是Drop-in UI初始化所必需的参数。Drop-in UI每次加载时，一定要使用新生成的client token。

Client token需要在服务器端生成，然后在网页端使用。

如果支付用户是一个新用户，生成client token的代码如下：

```ruby
get "/client_token" do
  @client_token = Braintree::ClientToken.generate
end
```

 **注** : 以上的代码是浏览器端通过ajax调用取得client token。另一种更常见的做法是直接在页面代码中使用，把client token存储在页面变量使用（类似于 <%= client_token %>）。

**文档**

* <https://developers.braintreepayments.com/start/overview#client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#get-a-client-token>
* <https://developers.braintreepayments.com/reference/request/client-token/generate/ruby>

### 客户端

#### 引用braintree.js

	<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
	<!-- 如果使用deviceData时出现javascript错误，使用如下的javacript文件引用，不要使用上面的
	<script src="https://js.braintreegateway.com/js/braintree-2.20.0.min.js"></script>
	-->

**文档**

* <https://developers.braintreepayments.com/guides/client-sdk/javascript/v2#use-the-direct-link>

#### 设置 

	<form id="checkout" method="post" action="/checkout">
	  <div id="payment-form"></div>
	  <input type="submit" value="Pay $10">
	</form>
	
	<script>
	  braintree.setup(
	    "<%= your_client_token %>",
	    "dropin", {
	      container: "payment-form",
	      dataCollector: {
		    kount: {environment: 'sandbox'}
		  },
		  onReady: function (braintreeInstance) {
		    var form = document.getElementById('payment-form');
		    var deviceDataInput = form['device_data'];

		    if (deviceDataInput == null) {
		      deviceDataInput = document.createElement('input');
		      deviceDataInput.name = 'device_data';
		      deviceDataInput.hidden = true;
		      form.appendChild(deviceDataInput);
		    }

		    deviceDataInput.value = braintreeInstance.deviceData;
		  }
	    });
	</script>

**文档**

* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#setup>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#get-a-client-token>
* <https://developers.braintreepayments.com/start/hello-client/javascript/v2#send-payment-method-nonce-to-server>
* <https://developers.braintreepayments.com/guides/advanced-fraud-tools/client-side/javascript/v2#collecting-device-data>

### 服务器端

#### 创建新交易

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_nonce => <nonce>,
	:options => {
	  :submit_for_settlement => true,
	  :store_in_vault_on_success => true
	},
	:device_data => params[:device_data]
)
```

* **[store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success)** - 此选项为TRUE时，交易时的用户数据还有支付方式将会存储在Braintree vault。Braintree会生成并返回customer id 和 payment method token （支付方式令牌） and return them in the response.
* **[submit_for_settlement](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.submit_for_settlement)** - enable this option to deduct fund from customer's credit card. Refer to <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement> for more information.

**文档**

* <https://articles.braintreepayments.com/get-started/transaction-life-cycle#submitted-for-settlement>


#### 处理交易结果

If the transaction is successful, the details of the transaction will be returned as part of the response.

```ruby
if result.success?
	transaction = result.transaction
	id = transaction.id
	braintree_customer_id = transaction.customer.id
	payment_method_token = transaction.payment_method_token
	
	#
	# save transaction id, customer id, payment method token
	#
end
```

* **[transaction id](https://developers.braintreepayments.com/reference/response/transaction/ruby#id)** - unique id of transaction
* **[customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id)** - When saving the customer details to vault, vault will generate a customer id. You can create a new _braintree_customer_id_ column in the Users table, and save the generated _braintree_customer_id_.
* **[payment method token](https://developers.braintreepayments.com/reference/response/transaction/ruby#credit_card_details.token)** - The payment method saved to vault will be assigned a payment method token

If the transaction is unsuccessful, you should check the error response code and handle the error properly. We recommend returning a generic error message to the customer and ask the customer to try again.

```ruby
if !result.success?
	response_code = result.error
	...
end
```

## Use Case - Existing customer creating a transaction

An existing customer is already associated with a Braintree [customer id](https://developers.braintreepayments.com/reference/response/transaction/ruby#customer_details.id), and vaulted payment methods.

### Generate Client Token

The client token is generated using the Braintree customer id, such that vaulted payment methods can be retrieved.

```ruby
get "/client_token" do
  @client_token = Braintree::ClientToken.generate(
	:customer_id => <a_customer_id>
  )
end
```

### Client Side

No change is required on the client side.

### Server Side

#### Create a new transaction

The only difference is the [store_in_vault_on_success](https://developers.braintreepayments.com/reference/request/transaction/sale/ruby#options.store_in_vault_on_success) does not need to be enabled this time.

```ruby
result = Braintree::Transaction.sale(
	:amount => <price>,
	:payment_method_nonce => <nonce>,
	:options => {
	  :submit_for_settlement => true
	},
	:device_data => params[:device_data]
)
```

#### Process the response

There is no need to retrieve customer id and payment method token.

```ruby
if result.success?
	transaction = result.transaction
	id = transaction.id
	
	#
	# save transaction id, customer id, payment method token
	# process transaction data
	#
end
```
